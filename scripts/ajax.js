/**
 * @desc AJAX function, prepare and send request to server at url
 * @param {string} type
 * @param {string} url
 * @param {function} callback
 */

function ajax(type, url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open(type, url);
 // xhr.open('GET\POST', 'https://asdas')
    xhr.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                callback(this.responseText);
            } else {
                console.error('Server returns a '+ this.status + ' status code. URL: "'+ url +'"');
                alert('Unexpected Error. ['+ this.status +']');
            }
        }
    };
    xhr.send(null);
}
